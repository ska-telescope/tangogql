DEVICE_NAME_QUERY = """query{devices(pattern: "sys/tg_test/1"){name}}"""

SINGLE_DEVICE_NAME_QUERY = """query{device(name: "sys/tg_test/1"){name}}"""

DEVICE_STATE_QUERY = """query{devices(pattern: "sys/tg_test/1"){state}}"""

CLASS_AND_DEVICE_QUERY = """ query{classes(pattern:"*"){
            name
            devices { name } } } """

DEVICE_PROPERTIES_QUERY = """query{devices(pattern: "sys/tg_test/1"){properties(pattern:
                       "Do_not_remove_this"){name,device,value}}}"""

DEVICE_ATTRIBUTES_QUERY = """ query{devices(pattern: "sys/tg_test/1"){attributes(pattern:"ampli"){
                                    name,
                                    device,
                                    datatype,
                                    format,
                                    dataformat,
                                    writable,
                                    label,
                                    unit,
                                    description,
                                    displevel,
                                    value,
                                    writevalue,
                                    quality,
                                    minvalue,
                                    maxvalue,
                                    minalarm,
                                    maxalarm,
                                    timestamp,
                                    enumLabels
                                    }}}"""

DEVICE_COMMANDS_QUERY = """ query{devices(pattern: "sys/tg_test/1"){commands(pattern:"DevBoolean"){
                                name,
                                tag
                                displevel,
                                intype,
                                intypedesc,
                                outtype,
                                outtypedesc}}} """

DEVICE_SERVER_QUERY = (
    """query{devices(pattern: "sys/tg_test/1"){server{id,host}}}  """
)

DEVICE_CLASS_QUERY = (
    """query{devices(pattern: "sys/tg_test/1"){deviceClass}}"""
)

DEVICE_PID_QUERY = """query{devices(pattern: "sys/tg_test/1"){pid}}"""

DEVICE_STARTED_DATE_QUERY = (
    """ query{devices(pattern: "sys/tg_test/1"){startedDate}} """
)

DEVICE_STOPPED_DATE_QUERY = (
    """ query{devices(pattern: "sys/tg_test/1"){stoppedDate}} """
)

DOMAIN_NAME_QUERY = """ query{domains(pattern: "*"){name}} """

DOMAIN_FAMILIES_QUERY = """query{domains(pattern: "sys"){
    families(pattern: "*"){
            name,
            domain,
            members(pattern:"*"){
                name,
            state,
            pid,
            startedDate,
            stoppedDate,
            exported,
            domain,
            family
            }
        }
  }
}
"""

MEMBER_NAME_QUERY = """
{
  members {
    name
  }
}
"""

MEMBER_STATE_QUERY = """
{
  members {
    state
  }
}
"""

MEMBER_DEVICE_CLASS_QUERY = """
{
  members {
    deviceClass
  }
}
"""

MEMBER_PID_QUERY = """
{
  members {
    pid
  }
}
"""

MEMBER_STARTED_DATE_QUERY = """
{
  members {
    startedDate
  }
}
"""

MEMBER_STOPPED_DATE_QUERY = """
{
  members {
    stoppedDate
  }
}
"""

MEMBER_EXPORTED_QUERY = """
{
  members {
    exported
  }
}
"""

MEMBER_DOMAIN_QUERY = """
{
  members {
    domain
  }
}
"""

MEMBER_FAMILY_QUERY = """
{
  members {
    family
  }
}
"""

# mutations
putDeviceProperty = """mutation{putDeviceProperty(device : "sys/tg_test/1" name: "sommar" value: "solig"){ok,
message}} """
deleteDeviceProperty = """mutation{deleteDeviceProperty(device : "sys/tg_test/1"
name: "sommar"){ok,message}}"""
executeDeviceCommand = """mutation{executeCommand(device : "sys/tg_test/1"
command: "DevBoolean" argin: 1){ok,message,output}}"""

executeDeviceCommand_wrong_input_type = """ mutation{executeCommand(device : "sys/tg_test/1"
command: "DevBoolean" argin: sdfsdf){
  ok,
  message,
output
}} """
executeDeviceCommand_none_exist_command = """ mutation{executeCommand(device : "sys/tg_test/1" command: "dfg" argin: 1){
  ok,
  message,
output
}} """

setAttributeValue = """mutation{setAttributeValue(device : "sys/tg_test/1" name: "ampli" value: 1){
  ok,
  message,

}}"""

setAttributeValue_wrong_input_type = """ mutation{setAttributeValue(device : "sys/tg_test/1" name: "ampli" value: dsf){
  ok,
  message,

}} """

setAttributeValue_none_exist_attr = """mutation{setAttributeValue(device : "sys/tg_test/1" name: "sdfa" value: 1){
  ok,
  message,

}} """

setAttributeValue_none_exist_device = """mutation{setAttributeValue(device : "sys/xfs/1" name: "ampli" value: 1){
  ok,
  message,

}} """
