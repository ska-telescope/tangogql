import builtins
import logging
import os
import unittest
from unittest import mock

# Create mock objects
mock_database = mock.Mock()
mock_pytango = mock.Mock()
mock_pytango.Database.return_value = mock_database

# Dictionary of modules to mock
mocked_modules = {
    "PyTango": mock_pytango,
    "tango": mock_pytango,
    "tangogql.tangodb": mock.Mock(),
    "tangogql.feature_flags": mock.Mock(),
    "tangogql.aioattribute.manager": mock.Mock(),
}

# Store the original __import__
original_import = builtins.__import__


class CustomImportContext:
    def __enter__(self):
        builtins.__import__ = custom_import

    def __exit__(self, exc_type, exc_val, exc_tb):
        builtins.__import__ = original_import


def custom_import(name, globals=None, locals=None, fromlist=(), level=0):
    if name in mocked_modules:
        return mocked_modules[name]
    if name.startswith("tangogql") and name != "tangogql.schema.base":
        # For submodules, we need to create the parent modules in the mocked_modules dict
        parts = name.split(".")
        for i in range(1, len(parts)):
            parent_module = ".".join(parts[:i])
            if parent_module not in mocked_modules:
                mocked_modules[parent_module] = mock.Mock()
        mocked_modules[name] = mock.Mock()
        return mocked_modules[name]
    return original_import(name, globals, locals, fromlist, level)


# Use the context manager to safely import the module we want to test
with CustomImportContext():
    from tangogql.schema.base import setup_logger


class TestSetupLogger(unittest.TestCase):

    def setUp(self):
        # Reset mocked modules before each test
        for module in mocked_modules.values():
            module.reset_mock()

    def mockenv(self, **envvars):
        return mock.patch.dict(os.environ, envvars)

    @mock.patch("os.path.exists")
    @mock.patch(
        "builtins.open",
        new_callable=mock.mock_open,
        read_data="logging: config",
    )
    @mock.patch("yaml.safe_load")
    @mock.patch("logging.config.dictConfig")
    def test_setup_logger_with_custom_config(
        self, mock_dict_config, mock_yaml_load, mock_file, mock_exists
    ):
        with self.mockenv(LOG_CFG="custom_logging.yaml"):
            mock_exists.return_value = True
            mock_yaml_load.return_value = {
                "version": 1,
                "disable_existing_loggers": False,
            }

            logger = setup_logger()

            self.assertIsInstance(logger, logging.Logger)
            self.assertEqual(logger.name, "tangogql.schema.base")
            mock_file.assert_called_once_with("custom_logging.yaml", "rt")
            mock_dict_config.assert_called_once_with(
                {"version": 1, "disable_existing_loggers": False}
            )

    @mock.patch("os.path.exists")
    @mock.patch("logging.basicConfig")
    def test_setup_logger_with_default_config(self, mock_basic_config, mock_exists):
        with self.mockenv():
            mock_exists.return_value = False

            logger = setup_logger()

            self.assertIsInstance(logger, logging.Logger)
            self.assertEqual(logger.name, "tangogql.schema.base")
            mock_basic_config.assert_called_once_with(level=logging.DEBUG)


if __name__ == "__main__":
    unittest.main()
