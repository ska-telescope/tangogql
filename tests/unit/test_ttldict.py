from collections.abc import Iterable
from unittest.mock import patch

import pytest

from tangogql.ttldict import TTLDict

NOW = 1700000000


@pytest.fixture
def dict_with_data():
    ttl_dict = TTLDict(None)
    ttl_dict["test1"] = "value"
    ttl_dict["test2"] = 123
    ttl_dict["test3"] = -123.123
    return ttl_dict


@pytest.fixture
def empty_dict():
    ttl_dict = TTLDict(11)
    return ttl_dict


def mock_time():
    return 1700000000


class TestTTLDict:
    def test_set_and_get_item(self, empty_dict):
        empty_dict["test1"] = "value"
        empty_dict["test2"] = 123
        empty_dict.__setitem__("test3", "Lorem Ipsum")
        empty_dict.__setitem__("test4", -123.123)
        assert "test1" in empty_dict._values.keys()
        assert "test2" in empty_dict._values.keys()
        assert "test3" in empty_dict._values.keys()
        assert "test4" in empty_dict._values.keys()
        assert empty_dict.__getitem__("test1") == "value"
        assert empty_dict.__getitem__("test2") == 123
        assert empty_dict["test3"] == "Lorem Ipsum"
        assert empty_dict["test4"] == -123.123

    def test_del_item(self, dict_with_data):
        del dict_with_data["test1"]
        assert "test1" not in dict_with_data._values.keys()

    def test_len_of_items(self, dict_with_data):
        assert len(dict_with_data) == 3

    def test_iter(self, dict_with_data):
        iter_obj = dict_with_data.__iter__()
        assert isinstance(dict_with_data, Iterable)
        assert next(iter_obj) == "test1"
        assert next(iter_obj) == "test2"

    def test_repr(self, dict_with_data):
        r = repr(dict_with_data)
        assert isinstance(r, str)

    @patch("time.time", mock_time)
    def test_set_and_get_ttl(self, dict_with_data):
        dict_with_data.set_ttl("test1", 10, NOW)
        dict_with_data.set_ttl("test2", 10, None)
        assert dict_with_data.get_ttl("test1", NOW) == 10
        assert dict_with_data.get_ttl("test2", None) == 10

    def test_expire_status(self, dict_with_data):
        dict_with_data.expire_at("test1", NOW - 1000)
        dict_with_data.expire_at("test2", NOW + 1000)
        dict_with_data.expire_at("test3", None)
        assert dict_with_data.is_expired("test1", NOW, False)
        assert dict_with_data.is_expired("test1", NOW, remove=True)
        assert "test1" not in dict_with_data.keys()
        assert not dict_with_data.is_expired("test2", NOW, False)
        assert not dict_with_data.is_expired("test2", NOW, False)
