from unittest.mock import MagicMock

from tangogql.feature_flags import FeatureFlags

CONFIG_WITH_FALSE_PUB_SUB_FLAG = {"publish_subscribe": "False"}
CONFIG_WITH_TRUE_PUB_SUB_FLAG = {"publish_subscribe": "True"}


def test_that_feature_flags_can_be_initialised():
    ff = FeatureFlags(CONFIG_WITH_FALSE_PUB_SUB_FLAG)
    assert ff.get_flag("publish_subscribe") is False

    ff = FeatureFlags(CONFIG_WITH_TRUE_PUB_SUB_FLAG)
    assert ff.get_flag("publish_subscribe") is True


def test_that_feature_flags_can_be_set():
    ff = FeatureFlags()
    ff.set_flag("publish_subscribe", "True")
    assert ff.get_flag("publish_subscribe") is True


def test_that_feature_flags_are_only_initialised_once():
    ff = FeatureFlags()
    ff.read_configs = MagicMock()
    ff.get_flag("publish_subscribe")
    ff.get_flag("publish_subscribe")
    ff.read_configs.assert_called_once()
