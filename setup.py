from setuptools import setup, find_packages

pkg_version = {}
with open("tangogql/version.py") as fp:
    exec(fp.read(), pkg_version)


def main():
    name = "tangogql"
    version = pkg_version["__version__"]
    description = "GraphQL interface for Tango."
    license = "GPLv3"
    url = "https://https://gitlab.com/MaxIV/web-maxiv-tangogql"
    packages = find_packages()
    python_requires = (
        ">=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*, !=3.5*"
    )
    setup(
        name=name,
        version=version,
        description=description,
        license=license,
        url=url,
        packages=packages,
        python_requires=python_requires,
        entry_points={"console_scripts": ["tangogql = tangogql.__main__:run"]},
        install_requires=[
            "aiohttp>=3.7.4",
            "aiohttp-cors>=0.7.0",
            "PyYAML>=5.4.1",
            "pytango>=9.3.3",
            "graphql_ws>=0.3.1",
            "graphene>=2.1.8",
            "pyjwt>=1.2.0",
        ],
    )


if __name__ == "__main__":
    main()
