Mutation
********

This module contains the GraphQL mutations available for managing device commands, attributes, and properties in the system.

.. automodule:: tangogql.schema.mutations
    :members:

Available Mutations
===================

ExecuteDeviceCommand
--------------------
Executes a command on a specified device.

**Arguments:**

- `device` (String, required): Name of the device on which the command will be executed.
- `command` (String, required): Name of the command to execute.
- `argin` (ScalarTypes, optional): Input argument for the command (can be string, int, bool, or float).

**Returns:**

- `ok` (Boolean): True if the command executes successfully, False otherwise.
- `message` (List of String): Success message or error message.
- `output` (ScalarTypes): Output of the command execution.

SetAttributeValue
-----------------
Sets a value to a specified attribute of a device.

**Arguments:**

- `device` (String, required): Name of the device.
- `name` (String, required): Name of the attribute.
- `value` (ScalarTypes, required): Value to be set (can be string, int, bool, or float).

**Returns:**

- `ok` (Boolean): True if the value is set successfully, False otherwise.
- `message` (List of String): Success message or error message.
- `value_before` (ScalarTypes): Value of the attribute before the mutation.
- `attribute` (DeviceAttribute): The attribute with the updated value.

PutDeviceProperty
-----------------
Adds a property to a specified device.

**Arguments:**

- `device` (String, required): Name of the device.
- `name` (String, required): Name of the property.
- `value` (List of String): Value of the property.

**Returns:**

- `ok` (Boolean): True if the property is added successfully, False otherwise.
- `message` (List of String): Success message or error message.

DeleteDeviceProperty
--------------------
Deletes a property from a specified device.

**Arguments:**

- `device` (String, required): Name of the device.
- `name` (String, required): Name of the property to be deleted.

**Returns:**

- `ok` (Boolean): True if the property is deleted successfully, False otherwise.
- `message` (List of String): Success message or error message.
