"""Module containing Queries."""

import fnmatch
import json
import logging as logger
import os
import re
import time
from collections import defaultdict, deque

from graphene import Boolean, Field, Float, Int, List, ObjectType, String
from tango import (
    CommunicationFailed,
    ConnectionFailed,
    DevFailed,
    DeviceUnlocked,
)

from tangogql.feature_flags import FeatureFlags
from tangogql.schema.attribute import DeviceAttribute
from tangogql.schema.base import db, proxies, subscriptions
from tangogql.schema.device import Device, DeviceCommand
from tangogql.schema.mutations import latest_mutation_result
from tangogql.version import __version__

latest_query_result = deque([])


def query_execution_time(f):
    def wrapper(self, info, *args, **kw):
        start = time.monotonic()
        response = f(self, info, *args, **kw)
        duration = time.monotonic() - start
        record = {
            "argument": json.dumps(kw),
            "duration": format(duration * 1000, ".2f"),
        }
        if len(latest_query_result) >= int(os.environ.get("METRICS_LIMIT")):
            latest_query_result.popleft()
        latest_query_result.append(record)
        return response

    return wrapper


class Member(Device):
    """This class represent a member."""

    domain = String()
    family = String()

    @property
    def info(self):
        """This method fetch a member of the device using the name of the
        domain and family.
        """

        if not hasattr(self, "_info"):
            # NOTE: If we want to keep it compatible with python versions lower
            #       than python 3.6, then use format ... buuuuuutttt,
            #       let's have some fun with the new f-strings
            devicename = f"{self.domain}/{self.family}/{self.name}"
            # noinspection PyAttributeOutsideInit
            self._info = db.get_device_info(devicename)
        return self._info


class Family(ObjectType):
    """This class represent a family."""

    name = String()
    domain = String()
    members = List(Member, pattern=String())

    def resolve_members(self, info, pattern="*"):
        """This method fetch members using the name of the domain and pattern.

        :param info:
        :param pattern: Pattern for filtering of the result.
                        Returns only members that match the pattern.
        :type pattern: str

        :return: List of members.
        :rtype: List of Member
        """

        members = db.get_device_member(f"{self.domain}/{self.name}/{pattern}")
        return [
            Member(domain=self.domain, family=self.name, name=member)
            for member in members
        ]


class Domain(ObjectType):
    """This class represent a domain."""

    name = String()
    families = List(Family, pattern=String())

    def resolve_families(self, info, pattern="*"):
        """This method fetch a list of families using pattern.

        :param info:
        :param pattern: Pattern for filtering of the result.
                        Returns only families that match the pattern.
        :type pattern: str

        :return:
            families([Family]):List of families.
        """

        families = db.get_device_family(f"{self.name}/{pattern}/*")
        return [Family(name=family, domain=self.name) for family in families]


class DeviceClass(ObjectType):
    name = String()
    server = String()
    instance = String()
    devices = List(Device)


# TODO: Missing documentation
class ServerInstance(ObjectType):
    """Not documented yet."""

    name = String()
    server = String()
    classes = List(DeviceClass, pattern=String())

    def resolve_classes(self, info, pattern="*"):
        """
        :param info: object
        :param pattern:
        """
        devs_clss = db.get_device_class_list(f"{self.server}/{self.name}")
        logger.debug(f"devs_clss: {devs_clss}")
        mapping = defaultdict(list)
        rule = re.compile(fnmatch.translate(pattern), re.IGNORECASE)

        for device, clss in zip(devs_clss[::2], devs_clss[1::2]):
            mapping[clss].append(Device(name=device))

        return [
            DeviceClass(
                name=clss,
                server=self.server,
                instance=self.name,
                devices=devices,
            )
            for clss, devices in mapping.items()
            if rule.match(clss)
        ]


class Server(ObjectType):
    """This class represents a query for server."""

    name = String()
    instances = List(ServerInstance, pattern=String())

    def resolve_instances(self, info, pattern="*"):
        """This method fetches all the instances using pattern.

        :param info:
        :param pattern: Pattern for filtering the result.
                        Returns only properties that matches the pattern.
        :type pattern: str

        :return: List of instances.
        :rtype: List of ServerInstance
        """

        instances = db.get_instance_name_list(self.name)
        rule = re.compile(fnmatch.translate(pattern), re.IGNORECASE)
        return [
            ServerInstance(name=inst, server=self.name)
            for inst in instances
            if rule.match(inst)
        ]


class ExecutionRecord(ObjectType):
    """This holds execution details of single query."""

    argument = String()
    duration = Float()


class ExecutionTime(ObjectType):
    """This contains query & mutation details."""

    mutation = List(ExecutionRecord)
    query = List(ExecutionRecord)


class SubscribedAttrs(ObjectType):
    """Data type for subscribed attributes."""

    name = String()
    attribute = String()
    listeners = Int()
    event_type = String()
    device_accessible = Boolean()


class Metrics(ObjectType):
    """Data type for metrics"""

    execution_time = Field(ExecutionTime)
    subscribed_attrs = List(SubscribedAttrs)
    pub_sub = Boolean()


class Query(ObjectType):
    """This class contains all the queries."""

    version = String()
    info = String()
    devices = List(Device, pattern=String())
    device = Field(Device, name=String(required=True))
    domains = List(Domain, pattern=String())
    families = List(Family, domain=String(), pattern=String())
    members = List(Member, domain=String(), family=String(), pattern=String())
    servers = List(Server, pattern=String())
    instances = List(ServerInstance, server=String(), pattern=String())
    classes = List(DeviceClass, pattern=String())

    attributes = List(DeviceAttribute, full_names=List(String, required=True))
    commands = List(DeviceCommand, full_names=List(String, required=True))

    metrics = Field(Metrics)

    async def resolve_version(self, info):
        return __version__

    # noinspection PyMethodMayBeStatic
    async def resolve_info(self, info):
        # refactored to use the reference to the DB set up by base rather than c
        # creating a new one (makes for easier unit testing).
        info = db.get_info()
        return info

    @query_execution_time
    async def resolve_device(self, info, name=None):
        """This method fetches the device using the name.

        :param info:
        :param name: Name of the device.
        :type name: str

        :return:  Device.
        :rtype: Device
        """
        # Check that the device exists in the database
        dev_info = db.get_device_info(name)
        return Device(name=dev_info.name)

    # noinspection PyMethodMayBeStatic
    @query_execution_time
    async def resolve_devices(self, info, pattern="*"):
        """This method fetches all the devices using the pattern.

        :param pattern: Pattern for filtering the result.
                        Returns only properties that matches the pattern.
        :type pattern: str

        :return: List of devices.
        :rtype: List of Device
        """
        device_names = await db.command_inout("DbGetDeviceWideList", pattern)
        return [Device(name=name) for name in device_names]

    # noinspection PyMethodMayBeStatic
    @query_execution_time
    async def resolve_attributes(self, info, full_names):
        result = []
        attr_list = defaultdict(list)
        logger.debug(f"Resolve attributes {full_names}")

        # Make attribute name lowercase to allow case insensitive comparison
        for name in full_names:
            *parts, attribute = name.split("/")
            device = "/".join(parts)
            attr_list[device].append(attribute.lower())

        for device, attrs in attr_list.items():
            try:
                proxy = proxies.get(device)
                attr_infos = proxy.attribute_list_query()

                for attr_info in attr_infos:
                    name = attr_info.name.lower()
                    if name in attrs:
                        result.append(
                            DeviceAttribute(
                                name=name,
                                device=device,
                            )
                        )
            except (
                DevFailed,
                ConnectionFailed,
                CommunicationFailed,
                DeviceUnlocked,
            ) as error:
                logger.error(
                    f"{device} :: Exception type\
                    {type(error)} in Query resolve_attributes"
                )
                logger.debug(f"{error}")

        return result

    # noinspection PyMethodMayBeStatic
    @query_execution_time
    async def resolve_commands(self, info, full_names):
        result = []
        cmd_list = defaultdict(list)

        for name in full_names:
            *parts, command_name = name.split("/")
            device_name = "/".join(parts)
            cmd_list[device_name].append(command_name)

        for device_name, command_names in cmd_list.items():
            proxy = proxies.get(device_name)
            cmd_infos = proxy.command_list_query()

            for cmd_info in cmd_infos:
                if cmd_info.cmd_name in command_names:
                    result.append(
                        DeviceCommand(
                            name=cmd_info.cmd_name,
                            tag=cmd_info.cmd_tag,
                            displevel=cmd_info.disp_level,
                            intype=cmd_info.in_type,
                            intypedesc=cmd_info.in_type_desc,
                            outtype=cmd_info.out_type,
                            outtypedesc=cmd_info.out_type_desc,
                            device=device_name,
                        )
                    )

        return result

    # noinspection PyMethodMayBeStatic
    @query_execution_time
    def resolve_domains(self, info, pattern="*"):
        """This method fetches all the domains using the pattern.

        :param info: details of the user, including name

        :param pattern: Pattern for filtering the result.
                        Returns only properties that matches the pattern.
        :type pattern: str

        :return: List of domains.
        :rtype: List of Domain
        """
        domains = db.get_device_domain("%s/*" % pattern)
        return [Domain(name=d) for d in sorted(domains)]

    # noinspection PyMethodMayBeStatic
    @query_execution_time
    def resolve_families(self, info, domain="*", pattern="*"):
        """This method fetches all the families using the pattern.

        :param info: details of the user, including name
        :param domain: Domain for filtering the result.
        :type domain: str

        :param pattern: Pattern for filtering the result.
                        Returns only properties that matches the pattern.
        :type pattern: str

        :return: List of families.
        :rtype: List of Family
        """

        families = db.get_device_family(f"{domain}/{pattern}/*")
        return [Family(domain=domain, name=d) for d in sorted(families)]

    # noinspection PyMethodMayBeStatic
    @query_execution_time
    def resolve_members(self, info, domain="*", family="*", pattern="*"):
        """This method fetches all the members using the pattern.

        :param info: details of the user, including name
        :param domain: Domain for filtering the result.
        :type domain: str

        :param family: Family for filtering the result.
        :type family: str

        :param pattern: Pattern for filtering the result.
                        Returns only properties that matches the pattern.
        :type pattern: str

        :return: List of members.
        :rtype: List of Domain
        """

        members = db.get_device_member(f"{domain}/{family}/{pattern}")
        return [
            Member(domain=domain, family=family, name=member)
            for member in sorted(members)
        ]

    # noinspection PyMethodMayBeStatic
    @query_execution_time
    def resolve_servers(self, info, pattern="*"):
        """This method fetches all the servers using the pattern.

        :param info: details of the user, including name,
        :param pattern: Pattern for filtering the result.
                        Returns only properties that matches the pattern.
        :type pattern: str

        :return: List of servers.
        :rtype: List of Server.
        """

        servers = db.get_server_name_list()
        # The db service does not allow wildcard here, but it can still
        # useful to limit the number of children. Let's fake it!
        rule = re.compile(fnmatch.translate(pattern), re.IGNORECASE)
        return [Server(name=srv) for srv in sorted(servers) if rule.match(srv)]

    # noinspection PyMethodMayBeStatic
    @query_execution_time
    def resolve_classes(self, info, pattern="*"):
        """This method fetches all the classes using the pattern.

        :param info: details of the user, including name,
        :param pattern: Pattern for filtering the result.
                        Returns only name that matches the pattern.
        :type pattern: str

        :return: List of classes.
        :rtype: List of classes.
        """
        result = []

        classes = db.get_class_list(pattern)
        for class_ in classes:
            class_devices = []
            devices = db.get_device_exported_for_class(class_)
            for device_ in devices:
                class_devices.append(Device(name=device_))
            result.append(DeviceClass(name=class_, devices=class_devices))
        return result

    async def resolve_metrics(self, info):
        """This returns the metrics data"""

        execution_time = {
            "query": latest_query_result,
            "mutation": latest_mutation_result,
        }
        subscribed_attrs = []
        devices_status = defaultdict(list)
        for attribute in subscriptions.attributes.values():
            device_accessible = True
            if devices_status.get(attribute.device) is not None:
                device_accessible = devices_status.get(attribute.device)
            else:
                try:
                    await attribute.device_proxy.ping()
                except DevFailed as err:
                    device_accessible = False
                    logger.info("Error while fetching device status: %s", err)
                devices_status[attribute.device] = device_accessible

            subscription = {
                "name": attribute.name,
                "attribute": attribute.attr,
                "listeners": len(attribute.listeners),
                "event_type": attribute.event_type,
                "device_accessible": device_accessible,
            }
            subscribed_attrs.append(subscription)
        toggle_flags = FeatureFlags()
        res = {
            "execution_time": execution_time,
            "subscribed_attrs": subscribed_attrs,
            "pub_sub": toggle_flags.get_flag("publish_subscribe"),
        }
        return res
